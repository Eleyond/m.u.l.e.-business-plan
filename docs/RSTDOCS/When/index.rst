.. _when:

When?
~~~~~~~~

.. contents::
   :local:

Timeline
===========

The timeline at this point is still hard to establish with certainty.
But I do believe the order of steps taken after establishing a team and acquiring infrastructure are as follows:


2018
-----

* Finishing business plan.
* Finding motivated partners and co-founders.
* Securing a first portion of capital.

2019
-----

* Acquiring agricultural property.
* Building the first AGV prototype.
* Building a simulator.
* Building a specific U W B protocol for positioning outside of the convex hull.
* Developing the interface, AGV pathfinding and AGV actions.
* Sensorfusion: ultrasonic sensors and odometery.
* Constructing the R&D lab, greenhouse and other test facilities.
* Creating the three test cases animal feeding outdoors, harvesting in greenhouses, moving fermenters in brewery.

2020
----

* Finishing the AGV to commercial level.
* Setting up the webstore, webpage, documentation and support channels.
* start marketing and (inbound) sales.
* Incorporate initial feedback and build modules.
* Document three success stories and do extensive marketing.
* Create interface for full autonomous navigation with P R O Z Y X.

2021
-----

* Final version of AGV.
* Adding modules to the design based on customer feedback.
* First sales from test sites (beer, hops).
* Planning of full scale production site.

.. _when_about:

About timing
===============

An important note on "time" and timing crucial to appreciating this business plan:

In his ted talk, start up guru Bill Gross reflected on 30 years of experience and expressed that timing is the single factor that will influence success the most.
For this he compared among others, z.com 1999-2003 which was heavily funded with youtube 2005 - present, where the former had the same idea and more cash but failed due to low broad-band penetration and lack of codec support in browsers.
Watch the full ted talk here: https://www.ted.com/talks/bill_gross_the_single_biggest_reason_why_startups_succeed/transcript#t-329526

.. figure:: /images/why/timing.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Bill Gross's estimation of start up success factors and their weight.


This is also exactly why think M.U.L.E. will do great. Our idea, the "what" is in itself not original and surely many will follow or already coming up with the same idea.
However right now the following is true:

* U W B-technology has only existed since this year on a scale that can be used industrially and is so novel the amount of facilities using it worldwide for automation can be counted on two hands.
* The Paris agreement presented in 2015 was approved in 2016, ratified by 125 countries in 2017 and will likely be put to practice in December 2018. The short to mid-term impact will likely be a carbon tax which will make centralised industrial farming much less economic due to the high level of emissions involved with both production and distribution. When this eventually happens, automated greenhouses will rapidly gain a competitive advantage.
* Lithium technology that enables smaller AGV's has only just started reaching its full potential with the advent of electric cars and is about to get a lot cheaper with Tesla's giga factories.
* Hydroponics and bato bucket technology is still novel.
* World population is growing steadily and food demand is increasing at an ever accelerating rate.
* Automation is just around the corner but the vast majority of warehouses, production sites and greenhouses has not been automated at all.
* Many experts believe water shortages and wars are imminent, with Syria having been just the first. Greenhouse production


.. figure:: /images/why/U W Bmarket.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Market of positioning solutions is rising fast.
