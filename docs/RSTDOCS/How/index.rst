.. _how:

How?
~~~~~


.. contents::
   :local:

.. _how_techdrivers:

Technological drivers
=======================

M.U.L.E. is only possible due to the combination of a number of very recent technological developments.
Below we will briefly look at each individual technology and its potential for inexpensive following robots.


U W B positioning
-----------------

U W B is a radio based technology that uses an infrastructure of static points "anchors" can position tags with 10 cm accuracy.
The technology is just like GPS, based on time of flight measurements, but works on a much smaller and more accurate scale.The chips to enable U W B positioning
on an affordable level are produced by a company called "DecaWrave" and have only been commercially available since 2015. Since, they have  already sold over a million units.

.. figure:: /images/why/chartcompare.png
    :width: 850px
    :align: center
    :alt: alternate text
    :figclass: align-center

    P R O Z Y X (U W B) is about 4 times less expensive than lidar, 20x more accurate than BLE.


P R O Z Y X's (and only a handful of notable competitors) U W B technology is now at the point where setups can scale with virtually no limit at price levels around 1 - 3 euro per m2 for open setups.


.. figure:: /images/why/tag.png
    :width: 250px
    :align: center
    :alt: alternate text
    :figclass: align-center

    P R O Z Y X tag is only 4 x 3 cm and can last up to 5 years on a single charge.

.. figure:: /images/why/abiGif.gif
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    P R O Z Y X forklifts in a warehouse of 30 000m2.

.. figure:: /images/why/257tags.gif
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    250 assets being tracked at the same time for an other case in agriculture.

Robot/ AGV technology
-----------------------------

The actual robot / AGV (Automated guided vehicle) is inspired by the hv-100, Stiga, kobuki, vacuumcleaner irobots and many more small to
medium sized autonomous robots. The bots themselves are still relatively expensive, but the component prices have gotten very low.

.. figure:: /images/why/gvgif.gif
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    HV-100 robots in action.


.. figure:: /images/why/hv100.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    The HV-100. Notice the Lidar sensor, which is the most expensive component also requiring the most processing power. We will replace this with a 50 euro U W B tag.

.. figure:: /images/why/stigarollerdesgin.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Stiga is one of the more popular outdoor lawn mowers and was a lead of P R O Z Y X. Notice the two-large wheel design with the front roller.


**Important design features:**

* Raspberry pi: using U W B instead of Lidar requires far less processing power and allows the use of a RBPI, which in turn will make it much easier to use other sensors and sell the robot to other RBPI enthousiasts.
* Two motor design: allows mobility with only two motors and no suspension or fly wheels. Simple and easy design with few parts.
* Two large wheels and one roller like the HV 100 and Stiga have, allow the robot to climb pretty much any surface without risk of falling. The rollers are made of nylon it seems, which is very tough and expensive and 3D-printable. Note the HV100 has a small roller on the back too, just in case.
* Geared motors: these motors are very inexpensive and very powerful. The downside is that they are very slow, but for our case that is totally ok.
* Wood/steel frame: we would like to make the frame at least partially out of wood. Current robot platforms are hard to modify. CNC'd wood is easy to produce in smaller volumes and very easy to modify later on. It also fits with the green approach and might make the robot more appealing as a prototyping platform for an extra revenue stream.
* Load capacity: 150 kg.

**Components:**

* Raspberry pi: 50,00 euros.
* Motor controllers (ESC): 50,00 euros.
* Raspberry pi touch screen: 80,00 euros.
* Battery pack: 250,00 euros.
* Charger: 40,00 euros.
* Steel under frame: 60,00 euros.
* 3D printed nylon brackets and roller: 50,00 euros.
* CNC Milled wood/ aluminum upper frame: 100,00 euros.
* Lights, ultrasonic sensors, camera: 150,00 euros.
* Big quality wheels that also work outdoors: 300,00 euros.
* Geared 48V DC motors: 200,00 euros.


Our total estimated production price is 1330,00 euros. This seems realistic when looking at similar finished platforms from alibaba not marketed towards a specific market.
This also offers tremendous opportunity for profit, knowing that the HV-100 retails for 20 000,00 - 30 000,00 USD.

.. figure:: /images/why/agv.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Chinese robot platform with similar components but different design and four motors for indicative pricing.


Blynk and Dash
-----------------

To facilitate development of M.U.L.E., we propose to focus primarily on backend development and use of the shelf cloud based tools for all
that concerns user interface, controls and dashboard. There are two really cool and quite novel tools we initially propose for this: dash and blynk.

Blynk was a kickstarter from 2015 and was the first platform of its kind. Blynk links development boards like arduino or raspberry pi directly
to a customisable smartphone or tablet app via their cloud server (which you can also run locally). Blynk started with simple button widgets
to control GPIO pins easily and remotely but quickly grew out to have features like digital joysticks, analogue sliders, video streams, email push notifications, graphs, terminals etc.
which all can be implemented with a single line of code. Even automatic CSV reports based on the status of the widgets is now a native feature.
Blynk has a great buesiness model and does not charge a subscription fee and only charges a one-time fee for the widgets you purchase. They can even be recycled at any time to buy different widgets.


.. figure:: /images/why/blynk.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Blynk apps are made by dragging and dropping widgets which can either be fed data, virtual pins or directly control physical GPIO's.

.. figure:: /images/why/blynk2.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Blynk apps can give a complete interface with multiple tabs and its appearance can be fully customised when used commercially.


Where blynk's focus is primarily on a direct interface and two-way communication, dash is an easy to use tool to make really detailed and good looking reports.
Dash works with python and provides a similar experience to pyplot, but it runs in browser and for a subscription fee can be accessed from anywhere through their cloud service.


.. figure:: /images/why/dash.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Blynk apps can give a complete interface with multiple tabs and can be fully customised when used commercially.


Tictail, helpscout, autotext enable, basecamp, public forum and read the "f*ing docs
-------------------------------------------------------------------------------------------

This business plan was entirely made through a service called "read the docs". This service allows simple markup language to magically transform into
an easy to maintain webpage. A similar service, tictail, is available for webshops at low cost including all payment services, inventory management, tracking and shipping management.
Helpscout is a superb tool for supporting clients and managing inbound email, accounts and communities. Basecamp is an excellent tool for project and team management.

At P R O Z Y X, none of these tools were present. Vast amounts of time were invested in creating these tools from scratch, not having an appropriate tool, or patching them when they broke.
Whilst these services are not free, the real time consuming part is realising that you need them and finding the good fits and learning to use them. This is fortunately experience we now have.

.. _how_bettermeans:

All parties are co-investors
=========================================================

In a typical start-up, success and risk is usually distributed as follows:

* Founders get all the shares but get waste deep in debt to be able to pay employees.
* As things start to go better, everyone earns a similar amount. If things go bad, the founders are screwed.
* If the company is successful, shareholders get 10 - 100 times more income than the average employee within first 5 - 7 years. It is also at this point that large wage discrimination based on the "marketability" or "hierarchy" of employees' skills and formal title come up.

M.U.L.E. is intended to be a team of 3 - 6 people, mostly engineers, developers and marketeers.
As all are highly educated and no manual labour is involved, this offers the unique opportunity to pay everyone equally much.
Equality in turn greatly simplifies alternative renumeration schemes.

The option we favor is to pay employees for their worked hours with a mix of income and shares (or options).
This has a number of advantages:

* Founders need less starting capital.
* Early employees and co-founders get a wage and must not survive on just a promise.
* Active long term employees naturally gain more shares in the company, and thus more profit and more voting rights.
* If the company does well, everyone does well. This in turn motivates. Profits when not re-invested, are directly distributed as dividend to all.
* Voting rights are awarded only to those people actively working in the company itself. No externals.
* The absence of external loans or funding creates a pleasant work environment without an incentive for short term money grabbing.

Each employee is thus compensated by the formula: base-pay * % full employment of which up to half is paid in wages.

.. note:: The core idea of this type of co-funding is that anyone at any time gets to "dilute" the shares/ create more shares simply by putting in labour or capital.

**Example part 1:**

Founder A and founder B start M.U.L.E.. Founder A inserts 25 000,00 euros of capital and works full time the first year at 50 000,00 euros/year of which 0% is paid in wages.
Employee B works one year full time for the same base pay and splits his remuneration 50/50 between wages and shares (or options).

At the end of year 1, 25 000,00 (capital invested) + 50 000,00 (invested labour founder A) + 25 000,00 (labour part of founder B not paid as wages)
= 100 000,00 euros is invested in M.U.L.E. due to either capital injection or devoted labour.

At the end of year one, founder 1 has shares equal to 75% and founder B has shares (or options) equal to 25% of the total of M.U.L.E..

**Example part 2:**

Founder C joins in year two and wants to invest 50 000,00 in capital or labor. This is added on to the previous investments of both capital and work.
Founder C will get 50 000,00 euros in shares or 33% of all shares. Founder A now owns 50% of all shares and founder B 17%.
Founders A and B do not mind that their portion got smaller relatively as in absolute terms it is still worth the same.


.. note:: There is a large fiscal advantage when freelancers/employees/co-founders choose shares over wages. Take for example an employee earning a yearly gross (brutto) income of 50K. Usually this will net him about +-30K due to taxes and fees. If that same employee choose to split his income 50/50 over shares and wages, he would get to keep the relatively much higher +- 19K/25K due to the progressive tax system in BE and NO. In addition the latter now has 25K in shares leading to a total wealth of 44K generated that year.


.. _how_commercialisation:

Commercialisation
=======================

The long term goal of M.U.L.E. is to create a platform for following robots, autonomous robots and raspberry PI robots.
We do not want to create finished solutions but a modular platform on which integrators can easily build or offer their own modules.


A great integrator / developer platform without active sales
------------------------------------------------------------------

P R O Z Y X was almost self-sufficient on developer kits for the first two years of its existence and to date, a large chunk of revenue comes from this market.
If we can conclude anything from this, it is that a good developer platform can really sell.

M.U.L.E. is aiming to make a type of robot/AGV (slow, medium-load, moderate size, U W B) that likely might be popular with a good niche group.
If we make the robot fully raspberry pi compatible, we could sell it through the hundreds of raspberry pi related resellers world wide.
The good thing about these is that merely attempting to resell our robots will cost very little, whilst succeeding might contribute a great deal.

P R O Z Y X in year one made 40 - 60K of the developer kit per month. In year two this continued with little growth with a team twice the size and in year three this
decreased even though the team had grown from 5 - 17 and 5 active people in operations/sales.

It should be clear from the onset that M.U.L.E. does NOT want to do any active sales. Only SEO, adwords, great docs and resellers.
We want to stay small and focus on our developer/integrator niche deliberately.

Test cases
-----------

It would be interesting to see if we could use the three test cases mentioned before to generate a permanent source of income.
A hydroponic hop greenhouse is said to be able to generate up to 50 000,00 euros per 100m2. If used in tandem with a brewery, this advantage might be leveraged.
Find more info on these on the :ref:`other` page.

.. figure:: /images/why/testcases.jpg
    :align: center
    :alt: Image failed to load
    :width: 60%

    Three test cases M.U.L.E. hopes to debute with and generate cashflow with.


Subsidy
-----------

In its first year of true operation, P R O Z Y X picked up a almost a million in subsidies whilst selling little more than a developer kit even after most of these subsidies had been been paid off.
In year two, we even found that there were subsidies for small things that needed almost no approval, like the flanders invest subsidies (only 10 - 20K per year, but still!).

With M.U.L.E. being in both technology and agriculture, I believe we could at the very least qualify for similar subsidies whilst also get subsidies for the sale of crops and investing in agricultural working materials.
A big point here, is that it would be very advantageous to split the company into a Belgian legal division and a Norwegian division to allow subsidies from both countries, along with the EU, to fund our development.

.. Note:: Having a Belgian branch of M.U.L.E. will allow us to apply for Belgian subsidies. There are people, like my former classmate Tatjana, who hunt subsidies for you on a "no cure, no pay" basis, making this very easy and risk free.

.. _how_cashflow:

Cashflow and funding
=====================

For a development timeline, consult the ":ref:`when`" page.

**Private investments:**

A number of key investments should be done privately and rented back to M.U.L.E.. The reason being that on the one hand, much farmland requires a person to live on it by law,
on the other hand, the possible dissolution of M.U.L.E. should  NOT result in the forceful sale of a home.

* 200 000,00 euros: A piece of land will need to be purchased of at least 2000m2, south-side sun all year and ideally space to house a team. Ideally this would be a Norwegian "Småbruk/small farm"but could also be an island or industrial patch of land. This will be purchased privately and rented to M.U.L.E.. consult the ":ref:`where`" page for a more detailed overview.
* 30 000,00 euros: A custom concrete foundation of 7,5*45m for the test greenhouse/lab will need to be constructed as mentioned in the ":ref:`how_techdrivers` - Modified hydroponic dutch bucket system" section.
* 40 000,00 euros: A greenhouse of 7 * 40 meters that offers space for an R&D lab, greenhouse (majority), brewery and possibly some housing. This will likely be:  http://norhage.no/produkt/drivhus/drivhus-tre/drivhus-tre-700-140m2-280m2/.


2019 Q3 - Q4 Prototype AGV and build test site
--------------------------------------------------

**Out:**

* 38 000,00 euros: development and engineering team, housing, transport (at least 3 members but not full time. 50K per memeber/year).
* 27 000,00 euros: (10%) rent of land, greenhouse and foundation.
* 12 000,00 euros: Investment equipment greenhouse and brewery section (bato buckets, central reservoir, perlite as growing medium).
* 12 000,00 euros: U W B equipment, support and license fees.
* 6 000,00 euros: Investment tools R&D lab.
* 3 000,00 euros: perishables like seeds, grains, rhizoma (for hop growing), fertiliser but also electricity and internet.
* 6 000,00 euros: prototype cost AGV (motors, raspberry pi's, motor drivers, milled frame).

Total: 104 000,00

**In:**

* 19 000,00 euros: Co-investor contribution. Also see ":ref:`how_bettermeans`".
* 12 000,00 euros: U W B manufacturer co-investment in goods.
* 27 000,00 euros: rent of land from founder.
* 46 000,00 euros: Private capital injection or loan.

Total: 104 000,00


2020 Finalise AGV and Software development
-------------------------------------------

**Out:**

* 150 000,00 euros: development and engineering team, housing, transport (at least equivalent of 3 FT members. 50K per member/year).
* 30 000,00 euros: Final development AGV and production of first batch.
* 20 000,00 euros: perishables like seeds, grains, rhizoma (for hop growing), fertiliser but also electricity and internet.

Total: 200 000,00

**In:**

* 75 000,00 euros: Co-investor contribution. Also see ":ref:`how_bettermeans`".
* 60 000,00 euros: Subsidies from BE, NO, EU. We will set up a Belgian and Norwegian branch to maximise this and will hire a dedicated consultant (Tatjana) to do this.
* 65 000,00 euros: Private capital injection or loan.

Total: 200 000,00


2021 Testing and optimisation
-----------------------------------------------------

**Out:**

* 150 000,00 euros: development and engineering team, housing, transport (at least equivalent of 3 FT members. 50K per member/year).
* 50 000,00 euros: perishables and production parts like seeds, grains, rhizoma (for hop growing), fertiliser but also electricity and internet.
* 10 000,00 euros: Purchase of farmbot hardware

Total: 210 000,00

**In:**

* 75 000,00 euros: Co-investor contribution. Also see ":ref:`how_bettermeans`".
* 60 000,00 euros: Subsidies from BE, NO, EU. We will set up a Belgian and Norwegian branch to maximise this and will hire a dedicated consultant (Tatjana) to do this.
* 65 000,00 euros: from sale of AGV, crops, automatically produced beer or entire test sites. see ":ref:`how_commercialisation`"
* 50 000,00 euros: Private capital injection or loan as buffer for subsidy and commercialisation.
* 50 000,00 euros: Commercialisation.

Total: 300 000,00

.. note:: Before we discussed possible inflows of cash in the first three years. Whilst this is likely, the plan cannot rely on it. This business will take considerable effort in the first three years. This is no "get quick rich scheme", which is why you might believe it could actually work. Hence the planning of capital investment for year three too.


