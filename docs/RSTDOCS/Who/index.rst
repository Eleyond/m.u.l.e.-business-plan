.. _who:

Who?
~~~~~

.. contents::
   :local:

Everyone is co-founder
============================

To recap what we discussed in the :ref:`how` section:

M.U.L.E. is intended to be a team of 3 - 6 people with each a mixed skill set, mostly engineers, developers and marketeers.
As all are highly educated and no manual labour is involved, this offers the unique opportunity to pay everyone equally much.
Equality in turn greatly simplifies alternative renumeration schemes.

The option we favor is to pay employees for their worked hours with a mix of competitive income (wage) and shares (or options).
This has a number of advantages:

* Founders need less starting capital.
* Early employees and co-founders get a wage and must not survive on just a promise.
* Active long term employees naturally gain more shares in the company, and thus more profit and more voting rights.
* If the company does well, everyone does well. This in turn motivates. Profits when not re-invested, are directly distributed as dividend to all.
* Voting rights are awarded only to those people actively working in the company itself. No externals.
* The absence of external loans or funding creates a pleasant work environment without an incentive for short term money grabbing.

Each employee is thus compensated by the formula: base-pay * % full employment of which up to half is paid in wages.

.. note:: The core idea of this type of co-funding is that anyone at any time gets to "dilute" the shares/ create more shares simply by putting in labour or capital.


2018 - 2020
============

Founders and people willing to do the dirty work.
-----------------------------------------------------

* Acquiring and modifying the property.
* Laying the lab and greenhouse foundation.
* Building the lab and greenhouse (or at least overseeing its build).
* Setting up the R&D lab and greenhouse infrastructure.
* Setting up the companies and laying the legal foundation for collaboration and co-founding.

Prototyping engineers
-----------------------

* Building the actual AGV prototype.
* Building the various modules.

Designer
---------

* Creating a look for M.U.L.E. that will create a buzz.
* Industrial design (fusion 360) and analysis of materials.
* designing modules.

Senior developers
----------------------

* Building a simulator.
* AGV pathfinding and control loops.
* U W B positioning outside of convex hull optimised for following.
* User Interface and data capture.
* Sensorfusion with ultrasonic and odometry

U W B expert
------------

* Installation of hardware.
* Optimisation of accuracy.
* Scheduling of multiple setups.
* virtual tag for autonomous navigation.
* Bundling offerings.

Additional
-----------

In addition to aforementioned:

* Horticulturist: Improve yield per m2.
* Hop consultant or expert on specific crop: Improve yield per plant.
* Marketeer: Launch initial commercialisation and spread company awareness.
* Algorithms/ AI specialist: Optimise.
* Operations: Manage inbound contact, accounts, purchases, accounting, suppliers and day to day tasks.
