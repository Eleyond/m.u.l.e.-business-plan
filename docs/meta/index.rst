Contributing
~~~~~~~~~~~~

Thanks to `the awesome theme this is forked from <https://sphinx-rtd-theme.readthedocs.io/en/latest/contributing.html>`_, changing both the theme and the documentation can be done in a very dynamic matter.

However, you'll need to install some things to enable this workflow, as you need to set up your environment as if you were a frontend developer.

Requirements
------------

You will need git, Python 3, Ruby, and NodeJS in your software stack.

Windows
=======

* `Install git <https://git-scm.com/download/win>`_
* `Install Python 3 <https://www.python.org/downloads/>`_
* `Install Ruby <https://rubyinstaller.org/>`_
* `Install NodeJS <https://nodejs.org/en/download/>`_ (LTS recommended)

.. note::
    
    After installing all this software, restart your system!

MacOS
=====

Probably same as Linux but with brew.

Linux
=====

Use your favorite package manager to install all of the above: git, Python 3, Ruby, and NodeJS.

Ubuntu
______


Arch
____


Additional commands
-------------------

You'll need to install these additional packages through your new software's package managers:

.. code:: bash

    pip install sphinx sphinxcontrib-httpdomain


.. code:: bash

    gem install sass


.. code:: bash

    npm install -g bower grunt-cli


.. code:: bash

    npm install


You might have to prepend these commands with `sudo` on a linux environment.

When this is done, you can start the auto-flow using this command:

.. code:: bash

    grunt


If you change the theme or the docs, the rendered website will auto-update, the site is hosted on localhost:1919.


Contact Laurent at laurent@pozyx.io or on Slack if you're encountering issues with running this.
