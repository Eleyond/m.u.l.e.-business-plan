.. _other:

Other
~~~~~~

Important ideas for test sites and agricultural ventures:

Hops
=======

In our research whilst writing this business plan, we discovered there was a novel project "hopshouse" that recently (2018) came up with a similar idea as ours but over in the US:
" Why not grow hops hydroponically in bato-buckets". They are a subsidiary of a large greenhouse builder "Rough brothers" and focus on the sale of the greenhouse, not the hops.
Mostly interesting is their ROI estimate which suggests yields of 80 - 140K euros per 100 m2, but also their idea of multiple harvests per year.
Below an extract of their brochure and estimate of return on investment per square foot:

.. figure:: /images/why/hopshouse1.jpg
    :align: center
    :alt: alternate text
    :figclass: align-center

    Hopshouse's info brochure page 3.

.. figure:: /images/why/hopshouse2.jpg
    :align: center
    :alt: alternate text
    :figclass: align-center

    Hopshouse's info brochure page 4.

.. figure:: /images/why/hopshouse3.jpg
    :align: center
    :alt: alternate text
    :figclass: align-center

    Hopshouse's info brochure page 5.

.. note:: In industrial outdoor farming, hops are cut down entirely all at once so the entire plant can be fed through a harvester. This saves on labour cost. Hopshouse presents the interesting idea that hops, like cucumbers and squashes,  can actually be harvested multiple times if picked manually. There is precedent for this. We have ordered the renowned classic book "Homegrown Hops: An Illustrated How to Do It Manual" to study this further but this claim seems valid. Pair this with robotised picking (which hopshouse is not doing nor would it work with their version of greenhouse) and harvest opportunity could indeed be huge. Alternatively it is possible that hopshouse means that the plants can be cut down and regrown multiple times a year like cannabis. Possibly both methods. We will enquire more with their sales team but this seems unlikely.


Beer
======

Hops are, as mentioned above, a very lucrative crop if grown successfully. Hydroponically grown hops can theoretically offer yields of up to 30 000,00 euros per 100m2 from their third year. Triple that if you believe the aforementioned brochure by "hopshouse".
There is no guarantee and we will study advanced hop growth extensively. But even in years one and two, the possibility exists to get hops with a retail value of 10 to 25 000,00 euros out of our research greenhouse.

If you read the sections above, you know that the first steps of our proposed growing process can be described as follows:

* A central reservoir with heating capabilities is placed in the middle.
* AGV's then fill up at the reservoir and distirbute their contents over individual 20l bato buckets.
* The central reservoir can also flow into return channels to heat the room for perfect climate control.
* The entire layout is organised so every cm2 can be reached, cleaned and disinfected by AGV's, all the time.

Now this process is almost exactly the same as the process for an automated beer brewery:

* A central boiler boils beer.
* AGV's fill up at the boiler and spread the beer over various 20 - 45l buckets, in this case fermenters.
* The near-boiling beer sterilises the fermenters and then cools in the fermenters.
* AGV's pour liquid yeast from a different reservoir in each fermenter and stir the beer.
* The central reservoir can also flow into return channels to heat the room for perfect climate control.
* The entire layout is organised so every cm2 can be reached, cleaned and disinfected by AGV's, all the time.

Now while the initial onset of M.U.L.E. is to automate greenhouses, not breweries, there is a lot of synergy possible.
Especially if the two reservoirs were in the same room and the fermenters were spaced between plants.
This is because growing is an endothermic process and brewing and fermenting is exothermic. The heat from the brew
could replace 100% of the energy needed to heat the greenhouse in winter.

Furthermore, hops at retail price may fetch 8 euros per 100 gram, but the distributor takes 50% margin.
Using the hops directly in Norway, would allow to sell beer with a profit of 1 - 2 euros per liter.
With hops being the most costly ingredient, this increases the possible profit margin from 30 000,00 to 60 000,00 euros per 100m2 in Norway, where beer prices are very high.
This revenue could fund further development and could even open up a secondary path for M.U.L.E..

.. note:: One limit to combining a greenhouse with a brewery in the same space, is that constant growing temperatures generally promote plant growth and not fruit growth. This means fermenting cannot be done between sign of first fruits til harvest. Another limitation might be food safety to get a brewing license.

.. figure:: /images/why/beeragv.png
    :align: center
    :width: 450 px
    :alt: alternate text
    :figclass: align-center

    Our basic AGV design repurposed for automated beer brewing.


Crop selection
================

Crop selection is an important factor in the feasibility and effiency of a greenhouse, especially an automated one.
There are four key factors:

* Economic viability.
* Annual vs Perrenial.
* Pollination.
* Determinate vs indeterminate.

Economic viability
---------------------

A quick indicative overview of consumer prices:

* Hops: 8 euros per 100 gram.
* Paprikas, peppers and chilis: 8 euros per 3kg.
* Tomato: 8 euros per 5kg.
* Potato and grain: 8 euros per 20 kg.

.. note:: As Andrew Mefferd points out in "The greenhouse and hoophouse grower's handbook", you can grow almost any crop better in a greenhouse. However it only makes economic sense for more delicate crops that fetch a high price per unit. Hydroponic potatoes are better, cleaner and more efficient in every way but nobody grows them because it makes no economic sense. Not surprisingly thus that hydroponics has been largely pushed by cannabis growers. Hops by the way are a relative of cannabis.

.. figure:: /images/why/potato.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Hydropnic potatoes are clean, large and easy to harvest.

Even though M.U.L.E. aims to reunite production with consumption locally, hop is the best crop for a first setup as it allows to get initial income from crop sales from a global market instead of a local market.
Revenue of 200m2 of greenhouse might be between 15 000,00 and 60 000,00 euros if successful.

Hops have almost no distribution cost due to their light weight/small quantities. Furthermore, if processed in beer and sold directly, they could yield up to double their consumer sales price.

In a later stage, the development of pepper's and cucumber would be the most lucrative. An important factor in automation is also that mature "fruits" must be
easily distinguishable for a computer. This is especially the case for peppers and tomatoes due to their color, but hops and cucumbers also likely fit this bill.

.. note:: One downside to hops is that they only start yielding their full potential after a few years, a strong plus is that the plants are perennial, meaning that unlike other greenhouse crops, they last for decades and do not need yearly propagation.

Perennial vs annual
--------------------
Perennials are plants that theoretically last forever, never die. Every spring they grow back and produce fruits and flowers.
The most relevant examples are hops, rhubarb, berries, radishes, artichoke, garlic, asperagus and almost fruits and olives.

Perennials are interesting for automation as they only need to be planted and propagated once. All though M.U.L.E. plans to use farmbot for automated propagation as mentioned before,
not having to make this investment in the first years is a distinct advantage. Futhermore, perennials are just over all less maintenance.

Thus here again, hop has a distinct advantage but expensive, delicate and popular fruits like avocado or mango would also be an interesting option to explore in colder climates.

Pollination
------------
A large portion of the appeal of an automated greenhouse is that it can completely be sealed of from the outside world.
All air intake can be filtered and human contact limited to an absolute minimum.
This creates a condition in which animals, insects or diseases are very unlikely to ruin the harvest (in outdoor growing these attribute to huge pesticide efforts and a large portion of failed harvests).
It would does be very suboptimal to have to open up the entire greenhouse for pollination after all this effort.

Fortunately, there are many crops that do not need pollination. Hops being one of them as hops without seeds are preferred. But other vegetables like cucumbers and tomatoes also come in modified hybrids that do not need pollination.

.. note:: Parthenocarpic cucumbers are hybrid plants that have been bred with emphasis on the incompletely dominant gene Pc, which is responsible for these cucumbers to produce fruit asexually. These cucumbers produce few or no seeds, any seeds that do ripen will not be fertile -- parthenocarpic cucumber seeds must be sourced yearly. Parthenocarpic plants' fruits can actually be damaged or deformed by pollination ad are bred specifically for the greenhousing industry.

Determinate vs Indeterminate
------------------------------

Many vegetables like tomato and cucumber come in two variants: determinate and indeterminate.
The difference is easiest understood as bush vs vine plants as seen in the figure below:

.. figure:: /images/why/deter1.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Determinate vs indeterminate plant. Notice that determinate has less yield potential but also requires no pruning or trellising.

Initially it seemed that determinate plants would be the best choice for greenhouse automation as they are mostly "fire and forget".
Once they are in their final container, these plants need no more attention and after harvest they can be cut down with zero extra work.

Indeterminate plants require pruning (trimming the vines in strategic places) and need a trellis, pole or rope to climb on. This is labour intensive.
Indeterminate plants however produce superior yield per plant, take less space horizontally and can be harvested over a much longer period of time.
This last part might be crucial for the homesteader or local communities living of the greenhouse's production or for large scale growing if AGV's cannot work fast enough to harvest
all the fruits in a span of a few days.

.. figure:: /images/why/deter2.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Determinate plants do have some strong drawbacks.

Note that hops are vining plants and do not come in determinate variants.
However, hops have the distinct advantage that they can be grown up a single thread of twine, making them pretty low maintenance too, but some human interaction will be needed.
Twine and hop are both bio-degradable and can be composted or fed to animals.

.. figure:: /images/why/hops.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Hops growing up twine.

Tech drivers in agriculture:
=============================

Open source CNC technology
-----------------------------

Farmbot, just like P R O Z Y X was an innovative kickstarter from a few years back.
Their bot's are relatively inexpensive. They cannot be used for large scale projects, but would be ideal to grow seedlings fully automatic.
An additional benefit is that their software and hardware is open-source. M.U.L.E. could use their hardware to build fully automated propagators.
The software M.U.L.E. would need is very similar to farmbot's as both are focused on managing plants based on coordinates. Possibly this open-source
software could also be helpful or at least an inspiration.

.. figure:: /images/why/farmbot.gif
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Farmbot in action watering seedlings with mind-numbing accuracy.

An other utility of farmbot is its long rails. A rail like that could be used vertically to bring a robot arm or even just linear actuator with hand to
an exact height. Now high crops are harvested with very expensive machinery. A farmbot rail could be attached to an AGV as it is relatively light.

.. figure:: /images/why/farmbotdraft2.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    A single farmbot rail with a a wheel could be attached to an AGV for mm accurate height movement of a robot arm on a very thin frame.

.. figure:: /images/why/crops.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    European research project "CROPS" in 2010 - 2014 actually had the same idea and made a fully working autonoumous harvester but this was before CNC technology became available with the rise of 3D printers.

.. figure:: /images/why/benomic.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Benomic "modern AGV" relying on a human to do the harvest. Due to it having to carry a human, it is very expensive to build and too wide for a hydroponic hop greenhouse.


C02 Generator
--------------

The injection of CO2 in a greenhouse can dramatically improve yield. To paraphrase Andrew Mefferd  in "The greenhouse and hoophouse grower's handbook",
you have to think of CO2 as fertiliser in the air. Fortunately there exist portable propane based CO2 injectors and CO2 tanks. In most current greenhouses,
these need to be placed in various places of the greenhouse to ensure even spread, have many fans or many nozzles. Our solution is inherently simpler:
just move the C02 generator around the greenhouse.

.. figure:: /images/why/C02inector.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Possible C02 generator on AGV design.

.. figure:: /images/why/co2generator.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Inexpensive CO2 generator.

.. note:: For now, propane is the cleanest way to introduce CO2 into a greenhouse save for CO2 tanks. But companies like "climeworks" are working on solutions to collect CO2 directly from the outside air.



Polycarbonate/wood greenhouse
------------------------------------

Greenhouse technology itself has also evolved quite a bit. A relatively new choice instead of glass and plastic foil/ bubble wrap, is polycarbonate.
Polycarbonate is a very strong plastic that that has huge air pockets in between for insulation. Think double glazing pushed to the limit.

.. figure:: /images/why/polyc.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Polycarbonate sheets offer superior insulation. Notice the air pockets of almost a full 1 cm.

Additionally, polycarbonate is opague instead of transparent, and diffuses the light so it would hit each plant equally.
The greenhouse we will build will thus use polycarbonate. There are some great manufacturers in Norway among which  is "Norhage".
Norhage makes greenhouses with full wooden frames, which are more sustainable, but also have a higher load capacity making them good for snowy winters.


.. figure:: /images/why/gh1.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Norhage greenhouse dimensions and frame.


.. figure:: /images/why/gh2.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Norhage wood and polycarbonate greenhouse setup.


Modified hydroponic dutch bucket system
------------------------------------------

Dutch bucket also known as "bato bucket" hydroponics is a relatively new implementation of hydroponics.
The idea is simple: you transfer your young plant (usually in a foam "starterplug") in a bed of rock (perlite) or clay pebbles in which its roots can anchor.

.. figure:: /images/why/dutchbucket.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    A dutch bucket hydroponic system in action.

Then you simply spray water/fertiliser mixture on the surface and let it slowly make its way along the roots back to the reservoir.
Part is absorbed by the roots, part drains back to the reservoir to then be fed back in to the dripline.


.. figure:: /images/why/batobucket.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    The traditional schematics of a dutch bucket system.

This may seem like a simple idea, but it offers some tremendous advantages over traditional greenhousing:

- You can grow much taller plants then regular hydroponics would allow because they can anchor in the bucket.
- Roots and plant stay dry most of the time, combating rot and blight.
- No soil is required, meaning reduced risk of contamination (disease or insect).
- Plants can be managed at the individual level. Great control and zero waste.
