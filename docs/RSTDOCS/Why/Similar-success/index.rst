.. _why_success:


Similar success stories:
~~~~~~~~~~~~~~~~~~~~~~~~~

.. contents::
   :local:

Hv-100
=========

Harvest automation made a robot, the hv-100, to automate the moving of growing buckets "pots" in plant nurseries, a very unrewarding, unhealthy and labour intensive job.
The hv-100 from Harvest automation uses an old technology (optical sensor to follow tape) along with a positioning technology that was novel in small scale
in 2014: lidar. With these two technologies combined and a simple but robust AGV design, Harvest automation has been able to sell these at 30 000,00 USD a piece.
The business case is simple: A worker costs 20 000,00 per year but the robot is way faster, more reliable and does not mind the tedious job of moving pots from one side of the field and back again, day in and day out.

Find more on their story here: https://www.youtube.com/watch?time_continue=18&v=YnFpZhzrAxc

.. figure:: /images/why/hv100.png
    :width: 850px
    :align: center
    :alt: alternate text
    :figclass: align-center

    HV-100 sales claim on website.

Projects CROPS and SWEEPER
===========================

Two consecutive projects funded by the EU and researched by EU universities and primarily Dutch companies.
The goal as to make automatic harvesters. In 2010 - 2014 they managed to use computer vision combined with a robotic arm to harvest automatically.

In 2015 - present (the project is still going), project sweeper has managed to make a completely autonomous AGV much in the way we envision it,
though currently it only works for yellow peppers in very specific environments.

Interesting about this project is that it proves M.U.L.E. is feasible.
Their website addresses much the same points as we do: AGV's can undercut labour cost.
Also interesting is that CROPS used exactly the same idea for harvesting Agre-e came up with: a single axis CNC "pole" on which a robot arm moves, attached to an AGV.
They seemed to have abandoned this approach in favor of a very expensive robot arm in the later SWEEPER,
but keep in mind that inexpensive stepper motors and belts only became available in 2013 - 2015 with the rise of consumer 3D printers.

.. figure:: /images/why/crops.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    European research project "CROPS"' harvester closely matches what we want to do with Farmbot's open source hardware.


You could argue that it's thus already been done and M.U.L.E. is late to the party. A counterargument would be that they seemed not to have used any affordable components nor spared any expense.
The robot arm used for example, retails for 30 000,00 euros without the controller and can carry 7k (a bit much to lift a single pepper?)
No pricing is available yet, its bulky and it is not working in real world environments yet (first demo event is 12/9/2018).

.. figure:: /images/why/sweeper.jpg
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Sweeper is an operational autonomous harvester AGV prototype.


Sweeper reminds me much of the EU subsidy AIRT project P R O Z Y X took part in, which served to prove that drones could fly autonomously using U W B.
After the AIRT project was completed successfully and the money had been cashed by all parties, there seemed no strategy to actually bring it to market.

Find the full video of CROPS here: http://www.crops-robots.eu/media/2014_04_29_PepperHarvestFestoGripper.mp4
Find the full video of SWEEPER here: https://www.youtube.com/watch?v=VbW1ZW8NC2E


Farmbot
========

Farmbot (https://farm.bot/) is an open source automation project that uses CNC technology. They have sold thousands of farmbots worldwide as kits.
Farmbot currently does not scale beyond 3 x 6 meters and is not at all economically feasible for commercial use, yet this has not stopped a massive international interest.

.. figure:: /images/why/farmbot-img2.jpg
    :width: 850px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Farmbot is just now releasing their second version: Farmbot genesis.


Notable P R O Z Y X uses in agricultural automation and AGV
=======================================================

P R O Z Y X over the past year has had a number of clients (some under NDA) who realised the same opportunities as M.U.L.E. has.
Take it not as competition, but as a sign that large industry professionals are also validating this path.

.. Note:: "a rising tide lifts all boats" - That the automated agriculture is gaining trajection and using U W B, is a clear sign that this business plan is looking in the right direction.

* A major Dutch producer of agricultural AGV's set to bring them to operations in 2019.
* A major Dutch producer of greenhouses in the Netherlands to use U W B for various automation.
* A small start up in strawberry harvesting with AGV's.

Other companies with related AGV automation projects:

* Cisco with P R O Z Y X powered AGV.
* Ninebot (Segway) with P R O Z Y X AGV.
* Amazon for warehouse automation with their familiar orange AGV's.
* Two major forklift manufacturers/ distributors.
* AB Inbev and ArcelorMittal for automated forklift mission planning.
* Various companies (the big ones) for AGV's in cleaning and lawn mowing.
* Google and other companies within electric car manufacturing and automated parking.
* Major car manufacturers and integrators for automated driving in the 5 - 10 year term.
* A company making AGV's for cleaning ice on ice skating tracks (in use).
* Artist Thijs Erens using P R O Z Y X to make interactive R/C car experiences with location triggers.