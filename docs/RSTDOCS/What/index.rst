.. _what:

What?
~~~~~~

.. contents::
   :local:

Intro
======


M.U.L.E (Short for "modular ultra-wide band lift and load engine) is a modular robotics platform that fulfills similar functions as a packmule would: follow a human, lift him and carry up to 150 kg.
To achieve this, M.U.L.E. combines the latest of U W B positioning technology (in which we have a huge advantage) with robotics, AGV and battery technology.

Our goal is to create an open source, modular, robotic (AGV, AMR, ROVER, other synonym etc.) platform that can follow a human operator from 50m away, with 10 cm accuracy.
M.U.L.E. Will be able to carry 150 kg at a speed of 6 km/h: enough to lift a human and up to 7 times what a trade union would allow him/her to carry.
M.U.L.E. is based on the popular raspberry pi platform and uses/ works with P R O Z Y X positioning hardware for optional full autonomous navigation of 5 - 20 cm accuracy.
A basic model that meets all these requirements and sells under 2500,00 euros, is our ultimate goal. M.U.L.E is to have the following features:


.. figure:: /images/why/features.jpg
    :align: center
    :alt: Image failed to load
    :width: 60%

    First concept feature overview.

Between 2013 to 2018, the cost of motors, controllers and batteries has fallen dramatically (just think how cheap hoverboards, drones and electric skateboards now are) but industrial positioning technologies are still expensive.
A commonly used AGV lidar system quickly costs 1500,00 euros. The inductive rails for industrial AGV's often run up from 10 to 100k in installation cost.
Because of this, most of the production sites I have studied and visited in my career either have full automation of all internal transport or none at all, where automation is reserved only for the very largest companies with the newest buildings.
This contrast is especially great in agriculture, where industrial farmers use large machines to harvest enormous mono-crop productions whilst greenhouses and traditional and organic farmers compete with unassisted manual labour.

.. figure:: /images/why/hardwarecomparison.png
    :align: center
    :alt: Image failed to load
    :width: 40%

    Comparison of hardware prices.

M.U.L.E. is to fill a large niche in the market for AGV's and automation by offering a robot that can follow a human operator with extreme precision, right out of the box whilst costing a fraction of industrial AGV's.
The primary market will be agriculture but the cases M.U.L.E. could fit are numerous:

* Following / lifting a person harvesting in a greenhouse, orchard, farm.
* Carrying tools for farming.
* Feeding, guiding, trapping and transporting animals.
* Moving fermenters in micro-breweries.
* Carrying heavy construction tools on a construction site.
* Following nurses in hospitals.
* Helping retailers move goods in store.
* Carrying gear on terrain only accessible by foot.
* Carrying water from wells in dry areas.
* Lifting painters, industrial cleaners etc.
* Mining: complementing the rail system in smaller shafts.
* Firefighting.
* Etc.

M.U.L.E. will be designed to be a modular platform to fit many use-cases, but with special intent for small scale agriculture and food production.
We believe that diversified local agriculture and food production is making a comeback. Local is by definition more competitive as it cuts out the cost for distribution and in the near future
also the cost for carbon tax and right to pollute. An inexpensive versatile following robot could give the much needed productivity boost in the present.

.. figure:: /images/why/agvplan.png
    :align: center
    :alt: Image failed to load
    :width: 60%

    First concept sketch.

Mission statement
====================

**Mission:** M.U.L.E. aims to make a general purpose robotic packmule platform that works out of the box, is affordable for small scale productions, open source and easy to modify. M.U.L.E's primary function is to follow a human operator with extreme precision, but optional autonomous navigation matters too.

Feasibility and inspiration
==============================

The idea for this project actually came from a client, named Todd. Many clients before him tried to create following robots for various "big" use-cases, but P R O Z Y X just was not
stable enough yet. Todd was fortunate enough to start exactly at the right time (firmware 1.3)and made a very accurate following robot easily able of carrying 100 kg using a few recycled electromotors, a P R O Z Y X loc kit and a wooden frame.
Todd made this robot for no commercial purpose in a few days from his garage. His latest version even has obstacle avoidance with the same ultrasonic sensors we aim to use.

.. figure:: /images/why/todd.png
    :align: center
    :alt: Image failed to load
    :width: 50%

    Find the full video of Todd's robot here: https://www.youtube.com/watch?v=ohVUM3QEt5Q


SWOT analysis
==============

Traditionally a good business plan has a SWOT (Strength, weakness, opportunity, threat) analysis.
Here is ours:

**Strengths:**

Strengths and weaknesses are of micro-economic nature.

* We have intimate knowledge of U W B positioning technology and early access to the latest innovations.
* We have experience with setting up a developer platform, documenting it and supporting it.
* There is presently no-one commercially doing the same, advantage of first to market.
* The concept has been proven by clients and its technical feasibility is certain.
* Initial investment is very low.

**Weaknesses:**

* The success of this project relies on some very specific people joining/ supporting it.
* Support of and a good relationship with a U W B integrator is crucial.
* M.U.L.E. will be very easy to replicate.

**Opportunities:**

Opportunities and threats are of macro-economic nature.

* There is a market for both raspberry Pi robot platforms as well as following robots and AGV development platforms. This market is not competitive and a niche.
* Chinese components are at an all time low in cost.
* U W B has only been useable for a few months. The timing is excellent, more on timing here: :ref:`when_about`
* Battery technology: pushed by Tesla and the car industry, batteries have never been more affordable.
* Rising energy prices and upcoming carbon tax make industrial and centralised production less viable, increasing viability of semi-automation.
* Climate change and climate extremes have a very negative impact on traditional and industrial farming.
* Overpopulation: a lot more people are going to need food from local production. This market can only grow.

**Threats:**

* U W B might nog get regulated for many of our use-cases.
* M.U.L.E. robots within their target group and price range might not meet safety and certification standards.
* Other companies are working on this with more resources. The EU project "Sweeper" is a good example. However, their focus is entirely on giant scale production.
* Much of the economic viability of automated greenhousing is based on the assumption that rising energy costs and a carbon tax will decrease the viability of the current centralised agriculture. This seems inevitable but the political climate may continue to allow unsanctioned pollution to continue for a few years to come.
* Much of the viability of automated greenhousing relates to labour cost. If somehow wages would continue to decrease instead of increase, competing with "economic slaves" might actually be hard.
* Economy of scale: our production capacity will be very limited at first compared to competitors.


Focus on usability in agriculture
==================================

Climate change, increasing energy prices, extreme weather a growing world population, water shortage and an impending carbon tax are going to make it very hard
for the current centralised "industrial farming" to keep up with the demand for food. Today 54% (68% by 2050) of the world's population lives in large cities and the survival of
these large cities relies completely on our ability to move food and other resources from one side of a country, or even the world, to the other side.
Presently, there is no accounting of the real cost of these logistics or the long term impact of our behaviour on the planet and its finite resources.
Detached from any environmental or moral arguments, this is an economic reality that cannot be sustained. It is our estimation that local food production
will be the fastest growing market in the decades to come.

Right now the bulk of the world's farming is done in three ways:

**Traditional and organic farming:**

* **What:** Using manual tools to work outdoor patches of land. No tractors but (albeit powered) hand carried tools.
* **Where:** This is what you see on organic farms in the West and most third world countries.
* **Advantages:** Traditional farming requires almost no capital investment, creates a high yield per m2 and can be done locally, close to where the food is consumed.
* **Disadvantages:** Extremely labour intensive. Very low success rate. A draught, disease, insect outbreak heavy rainfall, storm etc. can ruin an entire year's harvest in days.

**Industrial farming:**

* **What:** Using tractors with high tech attachments to do anything from plowing, seeding to harvesting with almost no manual labour.
* **Where:** The US is the world leader in this segment with the most advanced tractors and largest scale operations to leverage the economies of scale to the fullest extent.
* **Advantages:** Low labour cost, economy of scale.
* **Disadvantages:** Wasteful: uses extreme amounts of water, pesticide and fertiliser. Extremely high capital investment starting only in the millions. Very high logistics cost as industrial farming produces much more than local demand can keep up with. Still relatively low success rate.

**Greenhousing and protected culture:**

* **What:** Using plastics or glass to shield plants from all external elements and provide ideal growing conditions.
* **Where:** The Netherlands uses this method extensively and though being a small country, is the second largest exporter of vegetables and largest exporter of ornamental plants.
* **Advantages:** Can be done on a medium scale with moderate investment, very high success rate, very efficient: few input resources required. Very high yield per m2.
* **Disadvantages:** Extremely labour intensive and therefore only viable where cheap labour is easily available. Logistic cost is high as a workforce needs to be nearby production.


As summarised above (especially when done hydroponically) greenhouses can produce 10 - 20 times more food per square meter than industrial or even traditional farming.
Greenhouses provide a stable supply of food independent of weather, and can be operated year-round.
Additionally, greenhouses can be built on otherwise non-desirable pieces of land, further reducing the initial investment.
Greenhouses as well as organic forms require far less pesticides, water and fertiliser to produce the same yield, reducing the cost of production and likely increasing the quality and value of the crops.

.. figure:: /images/why/hydro.jpg
    :align: center
    :alt: Image failed to load
    :width: 50%

    Hydroponics vs outdoor farming.

Greenhouses however have one flaw: **labour cost**. Labour cost is by far the largest cost of any protected or smaller scale agricultural business.
A simple robot that carried tools, seedlings, harvest, and followed and lifted operators could create a huge boost in worker productivity leading to cost reduction and yield increase.

Greenhouse automation is already big business and all of the climate control, watering, fertilisation, CO2 injection and ventilation has been fully automated.
The recent developments of hydroponics, driven by the cannabis industry have made growing food almost entirely automated.

What has not been automated at all is transferring, trellising, pruning, harvesting and cleaning. Most efforts in this industry are done
by companies that make AGV's and robots that (semi-automatically) navigate on a rail like a train.
The goal of these producers is to sell to new giant greenhouses. The issue is that these industrial greenhouse systems start at 100K euros, which really isn't feasible for any kind of local food production.
M.U.L.E.'s goal is different. Instead of creating an entire infrastructure with traditional (expensive AGV's), we want to make a light following robot using only U W B and the latest
of brushless motor and battery technology. M.U.L.E. should be deployable in existing greenhouses big and small. So same idea but entirely different market.
To this end, M.U.L.E. will be deployed in three initial test cases:

.. figure:: /images/why/testcases.jpg
    :align: center
    :alt: Image failed to load
    :width: 60%

    Three test cases M.U.L.E. hopes to debute with and generate cashflow with.




