.. _where:

Where?
~~~~~~~~

.. contents::
   :local:

The ideal scenario would be to have a Norwegian farm with Norwegian agricultural subsidies and a partner Belgian tech company.

Why Norway?
============

The initial greenhouses, research and production will be setup in Hordaland, Norway.
Norway is the perfect place to launch a company in both robotics and agricultural automation because:

* Very high food prices and much import: beating the market here seems much more feasible than in countries with more industrial food production and lower incomes.
* Inexpensive land: Land in Norway outside of central hubs is very affordable. In a first phase, acquiring a small farm is very realistic and often cheaper than a city apartment. In later fases, there are entire islands and farming villages that have been abandoned over the past decades that could be re-purposed for automated food production.
* High distribution cost: Norway being the longest country in Europe has many areas where food needs to travel very far to reach the consumer. Automated local production is easy to justify.
* Culture of healthy and fresh food: Scandinavia has a very active and health-oriented population. Fresh foods are actively being pushed by both media, government and education.
* Most of the country is rocky and mountainous with very little fertile soil: terrible conditions for industrial farming but cheap land to construct greenhouses.
* Good fiscal climate: Taxes are much lower than Belgian taxes or many other European countries for that matter.
* Cheap electricity: Norway produces 100% green electricity mostly powered by hydro-energy. Automation can cut labour cost but not heating cost.
* Safe: both politically and when it comes to nature. Small chance of natural disaster or flooding.
* Climate change: Northern Europe is predicted to get significantly colder, not warmer due to climate change in the next 5 - 20 years. This will make outdoor agriculture very hard.
* GMO-free. Norway has one of the strictest regulations on GMO. This makes it harder and more expensive to farm outdoors where non-modified plants are most vulnerable.

Why Belgium?
===============

* My personal background along with that of many friends and hopefully partners.
* The headquarters of P R O Z Y X.
* Many subsidies to be had of which we and P R O Z Y X have gained intimate knowledge.
* Concentrated engineering/ developer network.


Small farm/ Småbruk
=====================

In Norway there is a concept known as the "Småbruk" or small farm. It is a farm, usually isolated by natural borders (like rocky soil or a steep hill or a fjord) from other land.
Its size is limited and cannot be expanded due to natural borders. The result is that it is relatively inexpensive to acquire one.

A Småbruk of 10 - 100K m2 (of which usually 10 - 20% is actually flat-ish) can be had in the 100 - 300K euro price range depending on location.
Even cheaper opportunities exist if you are willing to go isolated (which M.U.L.E. could do in the future).

The intention is to acquire one of these. It will make the ideal testing and production site.

.. figure:: /images/why/smabruk.png
    :width: 550px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Ad for a small farm in the desired area.
